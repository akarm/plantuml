====================================
 LPM Boarding Adapter Working Draft
====================================

Open Questions
==============

* How to model merchant data? Full merchant structure or a key-value list?

  * Probably a key-value list since it would not be hard to extend it and change in the future.

* Will LPM adapter have state? How to implement Ideal pool without state?

  * Argument against keeping state in girolink:
    it will make data migration for LPM adapter hard,
    since it will require coordination between two projects.

* How does LPM adapter get credentials to contact LPM (e.g. Sofort)?
  Girolink can pass it via ``partnerConfiguration``,
  but it will keep duplications (girogate also stores credentials).
  
* Authentication? Will the LPM Adapter be running in the same cluster? Should we use mutual TLS?

TODO
====

* Model errors

Workflows
=========

.. plantuml::
   :caption: Workflow sequence diagram

   participant girolink
   participant "LPM Adapter" as adapter
   participant Sofort #99FF99
   participant girogate

   girolink -> adapter : describe Sofort
   girolink <- adapter : description

   ...

   group Merchant Boarding
     girolink -> girolink: fills required data
     activate girolink
     girolink -> adapter : board
     activate adapter
     adapter -> Sofort : board
     activate Sofort
     adapter <- Sofort : project_id
     deactivate Sofort
     girolink <- adapter : config bindings
     deactivate adapter

     alt synchronous interface
     else asynchronous pulling interface
       girolink -> adapter: get status
       activate adapter
       adapter -> Sofort: get status
       activate Sofort
       adapter <- Sofort: pending
       deactivate Sofort
       girolink <- adapter: pending
       deactivate adapter
       ...5 minutes later...
       girolink -> adapter: get status
       activate adapter
       adapter -> Sofort: get status
       activate Sofort
       adapter <- Sofort: active
       deactivate Sofort
       girolink <- adapter: active
       deactivate adapter
     else asynchronous pushing interface
       adapter <-- Sofort: notify
       girolink <-- adapter: notify
       girolink -> adapter: get status
       activate adapter
       adapter -> Sofort: get status
       activate Sofort
       adapter <- Sofort: active
       deactivate Sofort
       girolink <- adapter: active
       deactivate adapter
     end

     girolink -> girogate : config bindings
     activate girogate
     girolink <- girogate : ack
     deactivate girogate
     deactivate girolink
   end

JSON Interface
==============

Every JSON request payload includes the ``operationalMode`` key, which
specifies the credential set to be used by the adapter. It has the following
values:

* ``live``: to be used in production deployments
* ``test``: to be used by everything else

Describe LPM Integration
------------------------
``POST <base_url>/`` describes the integration.

Request:

.. code:: js

  {
    "operationalMode": "test"
  }

Response:

.. code:: js

  {
    "partnerConfiguration": {
      "p24Channel": {
        "required": true
      }
    },

    "merchantData": {
      // The schema will predefine a list of possible keys.
      "legalEntityCountry": {
        "required": true,
        "allowedValues": ["uk", "de"]
      },
      "merchantTradeName": {
        "required": false,
        "regex": ".{3,100}"
      },
      "creditorId": {
        "required": true,
        "validationFunction": "validateCreditorId"
      }
    },
  
    "methods": {
      "board": {
        "interface": "pull" // synchronous, pull, push
      },
      "amend" {
        "interface": "pull"
      },
      "getStatus" {
        "interface": "synchronous"
      },
      "suspend" {
        "interface": "synchronous"
      },
      "resume" {
        "interface": "synchronous"
      }
    }
  }

Board a Merchant
----------------
``POST <base_url>/board`` boards a merchant on LPM.

Request:

.. code:: js

  {
    "operationalMode": "test",
    "partnerConfiguration": {
      "key": "value"
    },
    "merchantContractName": "STRIPEUKLIVEHAPPYHOMEG9",
    "merchantData": {
      "legalEntityCountry": "uk",
      "creditorId": "DE123456"
    }
  }

Response:

.. code:: js

  {
    "configBindings": {
      "key": "value"
    }
  }

Amend a Merchant
----------------
``PUT <base_url>/amend`` amends a merchant.

Request:

.. code:: js

  {
    "operationalMode": "test",
    "partnerConfiguration": {
      "key": "value"
    },
    "merchantContractName": "STRIPEUKLIVEHAPPYHOMEG9",
    "merchantData": {
      "legalEntityCountry": "uk",
      "creditorId": "DE123456"
    }
  }

Response:

.. code:: js

  {
    "configBindings": {
      "key": "value"
    }
  }

Get Status of a Merchant
------------------------
``POST <base_url>/status`` gives the status of a merchant.

Request:

.. code:: js

  {
    "operationalMode": "test",
    "partnerConfiguration": {
      "key": "value"
    },
    "merchantContractName": "STRIPEUKLIVEHAPPYHOMEG9"
  }

Response:

.. code:: js

  {
    "operationalStatus": "pending", // pending, active, suspended, failed
    "message": ""
  }

Suspend / Resume a Merchant on a Payment Method
-----------------------------------------------
``PUT <base_url>/suspend`` suspends a merchant on LPM.

Request:

.. code:: js

  {
    "operationalMode": "test",
    "partnerConfiguration": {
      "key": "value"
    },
    "merchantContractName": "STRIPEUKLIVEHAPPYHOMEG9"
  }

Response is empty.

Push Interface
--------------

LPM adapter will notify girolink by calling the endpoint: ``PUT /notification/{lpm_id}/{merchant_contract_name}``.

